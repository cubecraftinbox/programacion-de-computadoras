#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//ECUACION DE 2do GRADO [Z]TuX
float x2( float a, float b,float c )
{
 float j;
 j = (-b - sqrt(b*b - 4*a*c))/(2*a);
 return j;
}
float x1(float a, float b,float c)
{
 float k;
 k = (-b + sqrt(b*b - 4*a*c))/(2*a);
 return k; 
}

int main() {
 
 float a,b,c;
 printf ("RESOLVER ECUACIONES DE 2do GRADO\n");
 printf ("By [Z]tuX\n");
 printf ("La forma de la ecuacion es\n");
 printf ("ax2+bx+c=0\n\n");
 printf ("Introduce el valor para a: ");
 scanf ("%f",&a);
 printf ("Introduce el valor para b: ");
 scanf ("%f",&b);
 printf ("Introduce el valor para c: ");
 scanf ("%f",&c);
 if (((b*b)-4*a*c)<0)
 {
  printf("La Ecuacion NO tiene solucion\n");
 }
 else
  {
   printf("El valor para X1 es: %f",x1(a,b,c));
   printf("El valor para X2 es: %f",x2(a,b,c));
   printf("\n");
  }
 system("Pause");
 return 0;
}
