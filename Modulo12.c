#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> //sleep

#define SIZE 100
int person = 0;
int Index = -1;

typedef struct Info
{
	char name[32];
	char tell[32];
	int age;
}info; 

void wellcome()
{
	system("clear");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\t\t\t*******************************************\n");
	printf("\ t \ t \ t *************** Directorio telefónico simple ************ \ n");
	printf("\t\t\t*******************************************\n");
	printf("\ t \ t \ t 1. Agregar contacto 2. Eliminar contacto \ n");
	printf("\ t \ t \ t 3. Modificar contacto 4. Buscar contacto \ n");
	printf("\ t \ t \ t 5. Mostrar contactos 6. Salir \ n");
	printf("\t\t\t*******************************************\n");
	printf("\t\t\t*******************************************\n");
}

void searchname(info *card)
{
	int i;
	char name[32] = {0};
	Index = -1;
	printf("\ t \ t \ tPor favor ingrese su nombre:");
	scanf("%s", name);
	for (i = 0; i < person; i++)
	{
		if (strcmp(card[i].name,name) == 0)
		{
			Index = i;
			break;
		}
	}
}

void searchinfo(info *card)
{
	searchname(card);

	if ( Index == -1)
	{
		printf("\ t \ t \ t¡No se encontró a esa persona! \ n");
		sleep(1);
	}
	else
	{
		printf("\ t \ t \ tNombre:% s \ tTeléfono:% s \ tEdad:% d \ n", card[Index].name, card[Index].tell, card[Index].age);
		sleep(1);
	}
}

void addinfo(info *card)
{
	
	char name[32] = {0};
	char tell[32] = {0};
	int age = 0;
	searchname(card);
	if (Index != -1)
	{
		printf("\ t \ t \ t¡El contacto ya existe! \ n");
	}
	else
	{	
		printf("\ t \ t \ tPor favor ingrese su nombre, número de teléfono, edad:");
		scanf("%s%s%d", name, tell, &age);
		strcpy(card[person].name, name);
		strcpy(card[person].tell, tell);
		card[person].age = age;
		person++;
	}
}

void deleteinfo(info *card)
{
	int i;
	char ch = '\0';

	searchname(card);

	if (Index == -1)
	{
		printf("\ t \ t \ t¡No se encontró a esa persona! \ n");
		sleep(1);		
	}
	else
	{
		printf("\ t \ t \ tNombre:% s \ tTeléfono:% s \ tEdad:% d \ n", card[Index].name, card[Index].tell, card[Index].age);
		getchar();
		printf("\ t \ t \ t¿Eliminar? (s / n):");
		ch = getchar();

		if (ch == 'y')
		{
			if (Index != -1)
			{
				for (i = Index; i < person; i++)
				{
					strcpy(card[i].name,card[i + 1].name);
					strcpy(card[i].tell,card[i + 1].tell);
					card[i].age = card[i + 1].age;
				}
			}
			person--;
		}
	}

}

void modifyinfo(info *card)
{	
	char name[32] = {0};
	char tell[32] = {0};
	int age = 0;
	Index = -1;
	searchname(card);

	if (Index != -1)
	{
		printf("\ t \ t \ tPor favor ingrese un nuevo nombre, número de teléfono, edad:");
		scanf("%s%s%d", name, tell, &age);
		strcpy(card[Index].name, name);
		strcpy(card[Index].tell, tell);
		card[Index].age = age;
	}
	else
	{
		printf("\ t \ t \ t¡No se encontró a esa persona! \ n");
		sleep(1);
	}
 }



void showinfo(info *card)
{
	int i;
	for (i = 0; i < person; i++)
	{
		printf("\ t \ t \ tNombre:% s \ tTeléfono:% s \ tEdad:% d \ n", card[i].name, card[i].tell, card[i].age);
	}
	sleep(2);
	getchar();
	getchar();			//Pulse cualquier tecla para continuar
}

int main()
{
	wellcome();
	info card[SIZE] = {0};
	int choice = 0;


	while(1)
	{	
		printf("\ t \ t \ tPor favor, seleccione una función:");
		scanf("%d", &choice);
		switch (choice)
		{
			case 1:
				addinfo(card);
				break;
			case 2:
				deleteinfo(card);
				break;
			case 3:
				modifyinfo(card);
				break;
			case 4:
				searchinfo(card);
				break;
			case 5:
				showinfo(card);
				break;
			case 6:
				exit(0);
			default :
				printf("\ t \ t \ tLa entrada es incorrecta, ¡vuelve a ingresar! \ n");
				sleep(1);
		}
		wellcome();

	}
	return 0;
}
